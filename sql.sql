create table etudiant
(
    etudiantid serial not null
        constraint etudiant_pkey
            primary key,
    fname      varchar,
    lname      varchar,
    age        integer
);

alter table etudiant
    owner to postgres;

create table cours
(
    coursid     serial not null
        constraint cours_pkey
            primary key,
    sigle       varchar,
    name        varchar,
    description text
);

alter table cours
    owner to postgres;

create table inscription
(
    inscriptionid serial not null
        constraint inscription_pk
            primary key,
    date          date,
    etudiantid    integer,
    coursid       integer
);

alter table inscription
    owner to postgres;


